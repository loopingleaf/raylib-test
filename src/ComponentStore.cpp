#include <algorithm>

#include "Component.h"
#include "ComponentStore.h"

namespace ComponentStore
{
    namespace
    {
        std::map<ComponentId, ComponentTableBase> componentDatabase;
        ComponentId currentComponentId = COMPONENT_ID_NULL;
    }

    void store(const ComponentId& componentId, Component* component)
    {
        ComponentTableBase& table = componentDatabase[componentId];
        table[component->getEntityId()].push_back(component);
    }

    std::map<ComponentId, ComponentTableBase>& getDatabase()
    {
        return componentDatabase;
    }

    std::vector<Component*> getAllInEntity(const EntityId& entity)
    {
        std::vector<Component*> result;
        for(auto it : componentDatabase)
        {
            auto entityIt = it.second.find(entity);
            if(entityIt == it.second.end())
                continue;

            for(Component* component : entityIt->second)
                result.push_back(component);
        }
        return result;
    }

    ComponentId getNextId()
    {
        return ++currentComponentId;
    }
}
