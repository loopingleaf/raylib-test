/**
 * Inspired by Jeffery Myers https://github.com/JeffM2501/raylib_ecs_sample/blob/main/test/components.h
 */

#pragma once

#include <memory>
#include <string>
#include "imgui.h"
#include "Alias.h"

inline constexpr ComponentId COMPONENT_ID_NULL = 0;

#define DEFINE_COMPONENT(TYPE) \
    static ComponentId getBaseTypeId() { return reinterpret_cast<ComponentId>(#TYPE); } \
    static ComponentId getTypeId() { return reinterpret_cast<ComponentId>(#TYPE); }

#define DEFINE_COMPONENT_DERIVED(BASE, TYPE) \
    static ComponentId getBaseTypeId() { return reinterpret_cast<ComponentId>(#BASE); } \
    static ComponentId getTypeId() { return reinterpret_cast<ComponentId>(#TYPE); }

struct Component
{
    ComponentId id;

    explicit Component(const EntityId& entity) : id(COMPONENT_ID_NULL), m_entityId(entity) {}
    virtual ~Component() = default;

    static ComponentId getBaseTypeId() { return COMPONENT_ID_NULL; }
    static ComponentId getTypeId() { return COMPONENT_ID_NULL; }

    EntityId getEntityId() const
    {
        return m_entityId;
    }

#if (DEBUG_LEVEL >= 2)
    inline std::string stringId() { return std::to_string(id); }

    virtual void debugGui()
    {
        ImGui::BulletText("Component");
        ImGui::SameLine();
        ImGui::TextDisabled("?");
        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
            ImGui::TextUnformatted("This component type hasn't any defined debug ui");
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    virtual void debugDraw() {};
#endif

private:
    EntityId m_entityId;
};
