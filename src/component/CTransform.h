#pragma once

#include <string>
#include "raylib_namespace.h"
#include "Component.h"
#include "system/SDebug.h"

struct CTransform : public Component
{
    DEFINE_COMPONENT(CTransform);

    float rx, ry, scaleX, scaleY;
    rl::Vector2 position;

    explicit CTransform(const EntityId& entity, const rl::Vector2& position = { 0.f, 0.f},
                        const float& rotationX = 0.f, const float& rotationY = 0.f, const float& scaleX = 1.f,
                        const float& scaleY = 1.f)
        : Component(entity), rx(rotationX), ry(rotationY), scaleX(scaleX), scaleY(scaleY), position(position)
    {}

    [[nodiscard]] std::string toStr() const
    {
        return "x:" + std::to_string(position.x) + "|y:" + std::to_string(position.y);
    }

#ifdef DEBUG_TOOLS
    bool d_displayCross = false;

    void debugGui() override
    {
        const bool previousDisplayCross = d_displayCross;
        ImGui::PushID(DEBUG_GUI_ID(CTransform));
        ImGui::SetNextItemOpen(true, ImGuiCond_Once);
        if(ImGui::CollapsingHeader("Transform"))
        {
            float x = position.x;
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragFloat("##x", &x, 1.f, 0.f,
                             static_cast<float>(rl::GetScreenWidth()), "x: %.1f");

            float y = position.y;
            ImGui::SameLine();
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragFloat("##y", &y, 1.f, 0.f,
                             static_cast<float>(rl::GetScreenHeight()), "y: %.1f");

            ImGui::SameLine();
            ImGui::Text("Position");

            ImGui::SameLine();
            ImGui::Checkbox(( "Display##" + stringId() ).c_str(), &d_displayCross);
            position = rl::Vector2 { x, y };
        }
        ImGui::PopID();

        if(d_displayCross != previousDisplayCross)
        {
            if(d_displayCross)
                SDebug::addToDrawList(this);
            else
                SDebug::removeFromDrawList(this);
        }
    }

    void debugDraw() override
    {
        if(d_displayCross)
        {
            int x = static_cast<int>(position.x);
            int y = static_cast<int>(position.y);
            rl::DrawLine(x - 6, y, x + 5, y, rl::WHITE);
            rl::DrawLine(x, y - 6, x, y + 5, rl::WHITE);
        }
    }

#endif

};
