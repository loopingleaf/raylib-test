#pragma once

#include <cmath>
#include "Component.h"
#include "CTransform.h"
#include "Alias.h"
#include "Macros.h"
#include "ComponentStore.h"
#include "Entity.h"

struct CCelestial : public Component
{
    static const float G_CONST;
    float mass;
    rl::Vector2 velocity;

    CCelestial(const EntityId& entity) : Component(entity), mass(0.0f) {}

    CCelestial(const EntityId& entity, const float& mass) : Component(entity), mass(mass) {}

#if (DEBUG_LEVEL >= 2)

    void debugGui() override
    {
        ImGui::SetNextItemOpen(true, ImGuiCond_Once);
        if(ImGui::CollapsingHeader("Celestial"))
        {
            ImGui::DragFloat("Mass", &mass, 0.1f, 0.0f, 10000.f);
        }
    }

#endif
};

inline const float CCelestial::G_CONST = 0.3f;

enum class Direction {
    CLOCKWISE,
    COUNTERCLOCKWISE,
};

/// A CCelestial which only experience the gravitational force from one body,
/// for optimisation and consistency of the orbit of heavy objects, like planets.
/// The C2BodyCelestial's velocity can be calculated at any point of its orbit
/// and is guaranteed to never leave it.
///
/// @note Most of the credit for this code goes to Notovny for their GeoGebra
/// resource: https://www.geogebra.org/m/pyq6yc77
struct C2BodyCelestial : CCelestial
{
    EntityId orbitedBody;
    float orbitEccentricity;
    float orbitAngle;
    Direction orbitDirection;

    explicit C2BodyCelestial(const EntityId& entity)
        : CCelestial(entity), orbitedBody(ENTITY_ID_NULL), orbitEccentricity(0.f), orbitAngle(0.f),
        orbitDirection(Direction::CLOCKWISE)
    {}

    C2BodyCelestial(const EntityId& entity, const float& mass, const EntityId& orbitedBody,
                    const float& eccentricity = 0.f, const float& angle = 0.f,
                    const Direction& direction = Direction::CLOCKWISE)
        : CCelestial(entity, mass), orbitedBody(orbitedBody), orbitEccentricity(eccentricity), orbitAngle(angle),
        orbitDirection(direction)
    {}

    [[nodiscard]] float velocity() const
    {
        auto t = ComponentStore::get<CTransform>(getEntityId());

        /*if(orbitDirection == Direction::CLOCKWISE)
            result =
        else;*/
        return 0.f;
    }

#if (DEBUG_LEVEL >= 2)
    bool drawQ = false;
    bool drawMajorAxis = true;

    void debugGui() override
    {
        ImGui::SetNextItemOpen(true, ImGuiCond_Once);
        if(ImGui::CollapsingHeader("2 Body Celestial"))
        {
            ImGui::DragFloat("Mass", &mass, 0.1f, 0.0f, 10000.f);
            ImGui::DragFloat("Angle", &orbitAngle, 0.01f, 0.f, 2.f * PI);
            ImGui::DragFloat("Eccentricity", &orbitEccentricity, 0.005f, 0.f, 0.999f);

            ImGui::Checkbox("q", &drawQ);
            ImGui::SameLine();
            ImGui::BeginDisabled();
            auto q = majorAxisVertex1();
            ImGui::DragFloat("x", &q.x);
            ImGui::SameLine();
            ImGui::DragFloat("y", &q.y);

            auto Q = majorAxisVertex2();
            ImGui::DragFloat("x2", &Q.x);
            ImGui::SameLine();
            ImGui::DragFloat("y2", &Q.y);
            ImGui::EndDisabled();

            ImGui::BeginDisabled();

            auto fq = fqLength();
            ImGui::DragFloat("fq", &fq);

            auto a = semiMajorAxisLength();
            auto b = semiMinorAxisLength();
            ImGui::DragFloat("a", &a);
            ImGui::SameLine();
            ImGui::DragFloat("b", &b);

            //auto slope = tangentSlope();
            //ImGui::DragFloat("tangent slope", &slope);

            ImGui::EndDisabled();
        }

        if(drawQ)
        {
            rl::Vector2 pos = majorAxisVertex1();
            int x = static_cast<int>(pos.x);
            int y = static_cast<int>(pos.y);
            rl::DrawLine(x - 6, y, x + 5, y, rl::WHITE);
            rl::DrawLine(x, y - 6, x, y + 5, rl::WHITE);

            rl::Vector2 pos2 = majorAxisVertex2();
            int x2 = static_cast<int>(pos2.x);
            int y2 = static_cast<int>(pos2.y);
            rl::DrawLine(x2 - 6, y2, x2 + 5, y2, rl::WHITE);
            rl::DrawLine(x2, y2 - 6, x2, y2 + 5, rl::WHITE);

            rl::Vector2 pos3 = focus2();
            int x3 = static_cast<int>(pos3.x);
            int y3 = static_cast<int>(pos3.y);
            rl::DrawLine(x3 - 6, y3, x3 + 5, y3, rl::WHITE);
            rl::DrawLine(x3, y3 - 6, x3, y3 + 5, rl::WHITE);
        }

        if(drawMajorAxis)
        {
            rl::Vector2 q = majorAxisVertex1();
            rl::Vector2 angle { semiMajorAxisLength(), 0.f };
            angle = rl::Vector2Rotate(angle, -orbitAngle);
            auto dest = rl::Vector2Subtract(q, angle);
            rl::DrawLine( static_cast<int>(q.x), static_cast<int>(q.y),
                      static_cast<int>(dest.x), static_cast<int>(dest.y), rl::WHITE);
        }

        /*if(drawNormal)
        {
            Vector2 n = normal();
            Vector2 pos = ComponentStore::get<CTransform>(getEntityId())->position;
            DrawLine(pos.x, pos.y, pos.x + n.x, pos.y + n.y, WHITE);
        }*/
    }

#endif

//private:
    [[nodiscard]] rl::Vector2 focusBodyV() const // r->
    {
        const auto t1 = ComponentStore::get<CTransform>(getEntityId());
        const auto t2 = ComponentStore::get<CTransform>(orbitedBody);
        return rl::Vector2Subtract(t1->position, t2->position);
    }

    [[nodiscard]] float bodyAngle() const // θP
    {
        const auto v1 = focusBodyV();
        const auto v2 = rl::Vector2 { 1.f, 0.f };
        return Vector2Angle(v1, v2);
    }

    [[nodiscard]] float semiMajorAxisLength() const // a
    {
        const float rp = Vector2Length(focusBodyV());
        const float t = bodyAngle();
        return rp / ( (1.f - pow(orbitEccentricity, 2.f)) /
                      (1.f + orbitEccentricity * cos(t - orbitAngle)) );
    }

    [[nodiscard]] float semiMinorAxisLength() const // b
    {
        return std::sqrt(std::pow(semiMajorAxisLength(), 2.f) * (1 - std::pow(orbitEccentricity, 2.f)));
    }

    [[nodiscard]] float fqLength() const
    {
        const float r = (1.f - pow(orbitEccentricity, 2.f)) /
             (1.f + orbitEccentricity * cos(orbitAngle - orbitAngle));
        return semiMajorAxisLength() * r;
    }

    [[nodiscard]] rl::Vector2 majorAxisVertex1() const // q
    {
        auto orbitedT = ComponentStore::get<CTransform>(orbitedBody);
        auto v = rl::Vector2 { fqLength(), 0.f };
        v = Vector2Rotate(v, -orbitAngle);
        return Vector2Add(orbitedT->position, v);
    }

    [[nodiscard]] float fqLengthVertex2() const
    {
        const float r = (1.f - pow(orbitEccentricity, 2.f)) /
                        (1.f + orbitEccentricity * cos(PI + orbitAngle - orbitAngle));
        return semiMajorAxisLength() * r;
    }

    [[nodiscard]] rl::Vector2 majorAxisVertex2() const // Q
    {
        auto orbitedT = ComponentStore::get<CTransform>(orbitedBody);
        auto v = rl::Vector2 { fqLengthVertex2(), 0.f };
        v = Vector2Rotate(v, PI - orbitAngle);
        return Vector2Add(orbitedT->position, v);
    }

    [[nodiscard]] rl::Vector2 focus2() const
    {
        rl::Vector2 t { fqLength(), 0.f };
        t = Vector2Rotate(t, -orbitAngle);
        return Vector2Add(t, majorAxisVertex2());
    }

    void tangentSlope(const rl::Vector2& point, float& outSlope) const
    {
        auto p = Vector2Rotate(point, orbitAngle);
        if(p.x >= -0.0001f && p.x <= 0.0001f)
            return;

        outSlope = (std::pow(semiMajorAxisLength(), 2.f) / std::pow(semiMinorAxisLength(), 2.f))
                * (p.x / p.y);
    }

    /*[[nodiscard]] Vector2 normal()
    {
        Vector2 bodyPos = ComponentStore::getComponent<CTransform>(getEntityId())->position;
        Vector2 orbitedPos = ComponentStore::get<CTransform>(orbitedBody)->position;
        auto vF1body = Vector2Subtract(orbitedPos, bodyPos);
        auto vF2body = Vector2Subtract(focus2(), bodyPos);
        float fAngle = Vector2Angle(vF1body, vF2body);
        return Vector2Rotate( Vector2 { 1.f, 0.f }, fAngle );
    }*/

    /*[[nodiscard]] float tangentSlope()
    {

        return (( semiMinorAxisLength() * std::cos(bodyAngle()) * std::cos(-orbitAngle) ) -
            ( semiMajorAxisLength() * std::sin(bodyAngle()) * std::sin(-orbitAngle) )) /
            ( - ( semiMinorAxisLength() * std::cos(bodyAngle()) * std::cos(-orbitAngle) ) -
            ( semiMajorAxisLength() * std::sin(bodyAngle()) * std::sin(-orbitAngle) ));
    }*/

    float visViva(CCelestial* otherBody) const
    {
        auto transform = ComponentStore::get<CTransform>(getEntityId());
        auto otherTransform = ComponentStore::get<CTransform>(otherBody->getEntityId());
        auto r = rl::Vector2Distance(transform->position, otherTransform->position);
        return sqrt(G_CONST * otherBody->mass * ((2.f / r) - (1.f / semiMajorAxisLength())));
    }
};
