#pragma once

#include "ComponentStore.h"
#include "Entity.h"
#include "CTransform.h"
#include "CShape.h"

struct CEllipse : CShape {
    float angle;

    void draw() override {
        //auto transform = ComponentStore::get<CTransform>(getEntity()->getTypeId());
        DrawEllipseLines(200, 200,
                         sin(angle) * 200 + cos(angle) * 100,
                         cos(angle) * 200 + sin(angle) * 100,
                         rl::WHITE);
    }

#if (DEBUG_LEVEL >= 2)
    void debugGui() override {
        if(ImGui::CollapsingHeader("Ellipse"))
        {
            ImGui::DragFloat("Angle", &angle, 0.01f, 0.f, 2.f * PI);
        }
    }
#endif
};

