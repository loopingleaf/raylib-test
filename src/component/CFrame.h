#pragma once

#include "raylib_namespace.h"
#include "Alias.h"
#include "ComponentStore.h"
#include "CTransform.h"
#include "CDrawable.h"

class CFrame : public CDrawable
{
public:
    DEFINE_COMPONENT_DERIVED(CDrawable, CFrame)

    int width, height;
    int borderThickness = 1; // For now, the thickness can only be one
    rl::Color borderColor, color, outlineBorderColor;
    int offsetX;
    int offsetY;

private:
    float time;

public:
    explicit CFrame(const EntityId& entity)
            : CDrawable(entity), width(0), height(0), borderThickness(0),
              borderColor(rl::BLACK), color(rl::BLACK), outlineBorderColor(rl::BLACK), offsetX(0), offsetY(0)
    {
        time = 0.f;
    }

    void draw() override
    {
        auto t = ComponentStore::get<CTransform>(getEntityId());
        if(t == nullptr)
            return;

        time += rl::GetFrameTime();
        rl::DrawRectangle(std::round(t->position.x) + offsetX, std::round(t->position.y) + offsetY, width, height,
                          color);
        rl::DrawRectangleLines(std::round(t->position.x) + offsetX, std::round(t->position.y) + offsetY, width, height,
                               borderColor);
        rl::DrawRectangleLines(std::round(t->position.x) + offsetX + 1, std::round(t->position.y) + offsetY + 1,
                               width - 2, height - 2, outlineBorderColor);
        rl::DrawRectangleLines(std::round(t->position.x) + offsetX - 1, std::round(t->position.y) + offsetY - 1,
                               width + 2, height + 2, outlineBorderColor);
    }

#if(DEBUG_LEVEL >= 2)

    void debugGui() override
    {
        if(ImGui::CollapsingHeader(("Frame##" + stringId()).c_str()))
        {
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("##offsetX" + stringId()).c_str(), &offsetX, 1.f, INT_MIN, INT_MAX, "x: %i");
            ImGui::SetNextItemWidth(65.f);
            ImGui::SameLine();
            ImGui::DragInt(("##offsetY" + stringId()).c_str(), &offsetY, 1.f, INT_MIN, INT_MAX, "y: %i");
            ImGui::SameLine();
            ImGui::Text("Offset");

            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("Width##" + stringId()).c_str(), &width, 1.f, 0,
                           rl::GetScreenWidth());

            ImGui::SameLine();
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("Height##" + stringId()).c_str(), &height, 1.f, 0,
                           rl::GetScreenHeight());

            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("Border thickness##" + stringId()).c_str(), &borderThickness, 1.f, 0, 200);

            if(ImGui::ColorButton(("Background color##" + stringId()).c_str(), Color::RlToIm(color),
                                  ImGuiColorEditFlags_AlphaPreview, ImVec2(20, 20)))
                SDebug::displayColorPicker(&color);
            ImGui::SameLine();
            if(ImGui::SmallButton(("Background color##btn" + stringId()).c_str()))
                SDebug::displayColorPicker(&color);

            ImGui::SameLine();
            ImGui::TextDisabled("|");
            ImGui::SameLine();
            if(ImGui::ColorButton(("Border color##" + stringId()).c_str(), Color::RlToIm(borderColor),
                                  ImGuiColorEditFlags_AlphaPreview, ImVec2(20, 20)))
                SDebug::displayColorPicker(&borderColor);
            ImGui::SameLine();
            if(ImGui::SmallButton(("Border color##btn" + stringId()).c_str()))
                SDebug::displayColorPicker(&borderColor);

            if(ImGui::ColorButton(("Border outline color##" + stringId()).c_str(), Color::RlToIm(outlineBorderColor),
                                  ImGuiColorEditFlags_AlphaPreview, ImVec2(20, 20)))
                SDebug::displayColorPicker(&outlineBorderColor);
            ImGui::SameLine();
            if(ImGui::SmallButton(("Border outline color##btn" + stringId()).c_str()))
                SDebug::displayColorPicker(&outlineBorderColor);
        }
    }

#endif

};
