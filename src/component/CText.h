#pragma once

#include <mutex>
#include "raylib_namespace.h"
#include "Color.h"
#include "Component.h"
#include "CDrawable.h"

class CText : public CDrawable
{
public:
    DEFINE_COMPONENT_DERIVED(CDrawable, CText)

    inline static rl::Font font;
    inline static rl::Font vectorFont;

    std::string text;
    rl::Color color = Color::White;
    //float fontSize;
    float spacing;
    int offsetX = 0;
    int offsetY = 0;

private:
    inline static rl::Shader shader;

    std::once_flag initFlag;
    float seconds;

public:
    explicit CText(const EntityId& entity) : CDrawable(entity)
    {
        seconds = 0.f;
        std::call_once(initFlag, &CText::init);
    }

    inline rl::Vector2 getSize() const
    {
        rl::Vector2 result = rl::MeasureTextEx(font, text.c_str(), font.baseSize, spacing);
        return result;
    }

    void draw() override
    {
        auto t = ComponentStore::get<CTransform>(getEntityId());
        if(t == nullptr)
            return;

        seconds += rl::GetFrameTime();
        rl::SetShaderValue(shader, rl::GetShaderLocation(shader, "time"), &seconds, rl::SHADER_UNIFORM_FLOAT);
        rl::BeginShaderMode(shader);
        rl::DrawTextEx(font, text.c_str(), { std::round(t->position.x) + offsetX, std::round(t->position.y) + offsetY },
                       font.baseSize, 0, color);
        rl::EndShaderMode();
    }

private:
    static void init()
    {
        shader = rl::LoadShader(0, "../resources/shaders/chroma.fs");
        font = rl::LoadFontEx("../resources/ProggyClean.ttf", 13, 0, 0);
        vectorFont = rl::LoadFont("../resources/ProggyVector.ttf");
        //vectorFont = rl::LoadFontEx("../resources/ProggyVector.ttf", 12, 0, 0);
        rl::Vector2 resolution = rl::MeasureTextEx(font, "L", font.baseSize, 0);
        float screenSize[2] = { static_cast<float>(resolution.x), static_cast<float>(resolution.y) };
        rl::SetShaderValue(shader, rl::GetShaderLocation(shader, "resolution"), &screenSize, rl::SHADER_UNIFORM_VEC2);
        rl::SetTextureFilter(vectorFont.texture, rl::TEXTURE_FILTER_ANISOTROPIC_8X);
    }

#if(DEBUG_LEVEL >= 2)

public:
    void debugGui() override
    {
        if(ImGui::CollapsingHeader(( "Text (" + stringId() + ")" ).c_str()))
        {
            ImGui::InputText(("String##" + stringId()).c_str(), text.data(), text.length() + 1);
            if(ImGui::ColorButton(("Color##btn" + stringId()).c_str(), Color::RlToIm(color)))
                SDebug::displayColorPicker(&color);
            ImGui::SameLine();
            if(ImGui::SmallButton(("Color##" + stringId()).c_str()))
                SDebug::displayColorPicker(&color);
        }
    }

#endif
};