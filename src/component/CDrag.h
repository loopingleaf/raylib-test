#pragma once

#include <functional>

#include "raylib_namespace.h"
#include "imgui.h"
#include "Component.h"
#include "CTransform.h"

typedef struct RectangleI
{
    int x;                // Rectangle top-left corner position x
    int y;                // Rectangle top-left corner position y
    int width;            // Rectangle width
    int height;           // Rectangle height
} RectangleI;

class CDrag : public Component
{
public:
    DEFINE_COMPONENT(CDrag);

    RectangleI dragZone;

    /// To trigger a drag, the user must drag the component more than `deadZone`
    /// pixel of distance from the position of mouse press.
    int deadZone = 0;

    /// This component won't trigger onDrag and onDrop if dragAlone is set to true and another
    /// CDrag is already being dragged, even if this other component is still in its deadZone.
    //bool dragAlone = true;

private:
    bool beginDrag;
    bool isDragging;
    int mouseDownX, mouseDownY;
    int lastFrameMouseX, lastFrameMouseY;
    std::function<void(EntityId id, int dragDistanceX, int dragDistanceY)> onDragFunction;
    std::function<void(EntityId id, int dropPositionX, int dropPositionY)> onDropFunction;
    int dragDistanceX, dragDistanceY = 0;

public:
    explicit CDrag(const EntityId& entity)
            : Component(entity), beginDrag(false), isDragging(false)
    {}

    /// @return True if the component is being dragged, false otherwise
    bool update()
    {
        if(rl::IsMouseButtonPressed(rl::MOUSE_BUTTON_LEFT) && isInDragZone())
        {
            mouseDownX = lastFrameMouseX = rl::GetMouseX();
            mouseDownY = lastFrameMouseY = rl::GetMouseY();
            beginDrag = true;
        }
        if(!beginDrag)
            return false;

        if(rl::IsMouseButtonDown(rl::MOUSE_BUTTON_LEFT) &&
           (isDragging || std::abs(mouseDownX - rl::GetMouseX())
                          + std::abs(mouseDownY - rl::GetMouseY()) >= deadZone))
        {
            drag();
        }
        if(rl::IsMouseButtonReleased(rl::MOUSE_BUTTON_LEFT))
        {
            beginDrag = false;
            if(isDragging)
                drop();
        }
        return beginDrag;
    }

    /// Define the event that gets called every frame the component is dragged,
    /// if the drag distance is above deadZone.
    void onDrag(const std::function<void(EntityId id, int dragDistanceX, int dragDistanceY)>& function)
    {
        onDragFunction = function;
    }

    void onDrop(const std::function<void(EntityId id, int dropPositionX, int dropPositionY)>& function)
    {
        onDropFunction = function;
    }

private:
    bool isInDragZone() const
    {
        auto t = ComponentStore::get<CTransform>(getEntityId());
        int mX = rl::GetMouseX();
        int mY = rl::GetMouseY();
        bool inDragX = mX >= dragZone.x && mX <= (dragZone.x + dragZone.width);
        bool inDragY = mY >= dragZone.y && mY <= (dragZone.y + dragZone.height);
        if(t != nullptr)
        {
            int pX = t->position.x;
            int pY = t->position.y;
            inDragX = mX >= pX + dragZone.x && mX <= (pX + dragZone.x + dragZone.width);
            inDragY = mY >= pY + dragZone.y && mY <= (pY + dragZone.y + dragZone.height);
        }
        return inDragX && inDragY;
    }

    void drag()
    {
        isDragging = true;
        dragDistanceX = rl::GetMouseX() - lastFrameMouseX;
        dragDistanceY = rl::GetMouseY() - lastFrameMouseY;
        if(onDragFunction != nullptr)
            onDragFunction(getEntityId(), dragDistanceX, dragDistanceY);
        lastFrameMouseX = rl::GetMouseX();
        lastFrameMouseY = rl::GetMouseY();
    }

    void drop()
    {
        dragDistanceX = 0;
        dragDistanceY = 0;
        isDragging = false;
        if(onDropFunction != nullptr)
            onDropFunction(getEntityId(), rl::GetMouseX(), rl::GetMouseY());
#if (DEBUG_LEVEL >= 2)
        lastDroppedX = rl::GetMouseX();
        lastDroppedY = rl::GetMouseY();
#endif
    }

#if (DEBUG_LEVEL >= 2)

private:
    int xDistanceBuffer = 0, yDistanceBuffer = 0;
    float bufferTimer = 0.f;
    int lastDroppedX = 0, lastDroppedY = 0;

public:
    void debugGui() override
    {
        if(ImGui::CollapsingHeader(("Drag##" + stringId()).c_str()))
        {
            bufferTimer += rl::GetFrameTime();
            if(bufferTimer >= 2.f)
            {
                bufferTimer = 0.f;
                xDistanceBuffer = 0;
                yDistanceBuffer = 0;
            }

            ImGui::BeginDisabled(true);
            ImGui::Checkbox(("##IsdraggingBox" + stringId()).c_str(), &isDragging);
            ImGui::EndDisabled();
            ImGui::SameLine();
            ImGui::Text("Is dragging");

            ImGui::BeginDisabled(true);
            ImGui::SetNextItemWidth(65.f);
            xDistanceBuffer = isDragging ? xDistanceBuffer + dragDistanceX : 0;
            ImGui::DragInt(("##xDragged" + stringId()).c_str(), &xDistanceBuffer, 0, 0, 0, "x: %i");
            ImGui::SameLine();
            ImGui::SetNextItemWidth(65.f);
            yDistanceBuffer = isDragging ? yDistanceBuffer + dragDistanceY : 0;
            ImGui::DragInt(("##yDragged" + stringId()).c_str(), &yDistanceBuffer, 0, 0, 0, "y: %i");
            ImGui::EndDisabled();
            ImGui::SameLine();
            ImGui::Text("Drag distance");

            ImGui::BeginDisabled(true);
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("##xDropped" + stringId()).c_str(), &lastDroppedX, 0, 0, 0, "x: %i");
            ImGui::SameLine();
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("##yDropped" + stringId()).c_str(), &lastDroppedY, 0, 0, 0, "y: %i");
            ImGui::EndDisabled();
            ImGui::SameLine();
            ImGui::Text("Drop position");

            ImGui::DragInt(("Dead zone##" + stringId()).c_str(), &deadZone, 1.f, 0, INT_MAX);
        }
    }

#endif
};
