#pragma once

#include "Component.h"

class CDrawable : public Component
{
public:
    DEFINE_COMPONENT(CDrawable)

    CDrawable(const EntityId& entity) : Component(entity)
    {}

    virtual void draw()
    {}
};
