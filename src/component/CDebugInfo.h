#pragma once

#if (DEBUG_LEVEL >= 2)
#include <string>
#include "Component.h"

class CDebugInfo : public Component
{
public:
    DEFINE_COMPONENT(CDebugInfo)

    int* width;
    int* height;

private:
    inline static std::map<std::string, int> nameIds;
    std::string name;
    int nameId = 0;

public:

    explicit CDebugInfo(const EntityId& entity) : Component(entity)
    {}

    std::string getName() const
    {
        return name + "_" + std::to_string(nameId);
    }

    void setName(const std::string& nameToSet)
    {
        if(name == nameToSet)
            return;

        name = nameToSet;
        if(nameIds.contains(name))
        {
            nameId = ++nameIds[name];
        }
        else
        {
            nameIds[name] = 1;
            nameId = 1;
        }
    }

    void debugGui() override {}
};
#endif
