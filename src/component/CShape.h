#pragma once

#include "Component.h"

struct CShape : Component {
    DEFINE_COMPONENT(CShape)

    explicit CShape(const EntityId& entity) : Component(entity)
    {}

    virtual void draw() = 0;
};

struct CRectangle : CShape {
    DEFINE_COMPONENT_DERIVED(CShape, CRectangle)

    RectangleI rectangle { 0, 0, 0, 0 };
    rl::Color color = rl::WHITE;

    explicit CRectangle(const EntityId& entity) : CShape(entity)
    {}

    void draw() override
    {
        auto t = ComponentStore::get<CTransform>(getEntityId());
        if(t == nullptr)
            return;

        rl::DrawRectangle(t->position.x + rectangle.x, t->position.y + rectangle.y,
                          rectangle.width, rectangle.height, color);
    }
};