#include "CAnimation.h"
#include "raylib_namespace.h"
#include "system/SDebug.h"

CAnimation::~CAnimation()
{
#ifdef DEBUG_TOOLS
    delete[] curveXs;
    delete[] curveYs;
#endif
}

void CAnimation::play()
{
    if(!isPaused)
        return;

    if(time == 0.f && onBeginFunction != nullptr)
        onBeginFunction(this);
    isPaused = false;
}

void CAnimation::pause()
{
    if(isPaused)
        return;

    isPaused = true;
}

void CAnimation::stop()
{
    stopNextFrame = true;
}

void CAnimation::addKeyframe(const Keyframe& kf)
{
    keyframes.push_back(kf);
}

void CAnimation::onBegin(const std::function<void(CAnimation*)>& function)
{
    onBeginFunction = function;
}

void CAnimation::onEnd(const std::function<void(CAnimation*)>& function)
{
    onEndFunction = function;
}

void CAnimation::onPlay(const std::function<void(EntityId, float)>& function)
{
    onPlayFunction = function;
}

void CAnimation::update(float dt)
{
    if(keyframes.empty() || isPaused && !stopNextFrame)
        return;

    if(stopNextFrame)
    {
        stopNextFrame = false;
        isPaused = true;
        time = 0.f;
    }
    else
        time += dt;
    updateCurrentKeyframe();
    if(onPlayFunction != nullptr)
        onPlayFunction(getEntityId(), getCurrentKeyframeValue());
}

float CAnimation::getDuration()
{
    float result = 0.f;
    if(keyframes.empty())
        return result;

    for(int i = 0; i < keyframes.size(); ++i)
    {
        result += keyframes[i].timestamp;
    }
}

void CAnimation::updateCurrentKeyframe()
{
    float calcTime = 0.f;
    int i = 0;
    while(calcTime <= time && i < keyframes.size())
    {
        calcTime += keyframes[i].timestamp;
        ++i;
    }
    currentKeyframeId = i - 1;
    if(calcTime <= time)
        currentKeyframeTime = keyframes[currentKeyframeId].timestamp;
    else
        currentKeyframeTime = calcTime - keyframes[currentKeyframeId].timestamp + time;
}

float CAnimation::getCurrentKeyframeValue()
{
    if(keyframes.empty())
        return 0.f;

    if(keyframes.size() == 1 && keyframes[0].timestamp == 0.f)
        return keyframes[0].value;

    float previousKfValue;
    if(currentKeyframeId == 0)
        previousKfValue = keyframes[0].timestamp == 0.f ? keyframes[0].value : 0.f;
    else
        previousKfValue = keyframes[currentKeyframeId - 1].value;
    float normalizedValue = rl::Normalize(currentKeyframeTime, 0.f, keyframes[currentKeyframeId].timestamp);
    float amountOnCurve = keyframes[currentKeyframeId].easingCurve(normalizedValue);
    float result = rl::Lerp(previousKfValue, keyframes[currentKeyframeId].value, amountOnCurve);
    return result;
}

float CAnimation::getKeyframeValue(float atTime)
{
    if(keyframes.empty())
        return 0.f;

    if(keyframes.size() == 1 && keyframes[0].timestamp == 0.f)
        return keyframes[0].value;

    if(atTime == 0.f)
    {
        if(keyframes.empty() || keyframes[0].timestamp != 0.f)
            return 0.f;
        return keyframes[0].value;
    }

    float calcTime = 0.f;
    int i = 0;
    while(calcTime <= atTime && i < keyframes.size())
    {
        calcTime += keyframes[i].timestamp;
        ++i;
    }
    int kfId = i - 1;

    float timestamp;
    if(calcTime <= atTime)
        timestamp = keyframes[kfId].timestamp;
    else
        timestamp = calcTime - keyframes[kfId].timestamp + atTime;

    float previousKfVal = 0.f;
    if(kfId != 0)
        previousKfVal = keyframes[kfId - 1].value;

    float normalizedTimestamp = rl::Normalize(timestamp, 0.f, keyframes[kfId].timestamp);
    float amountOnCurve = keyframes[kfId].easingCurve(normalizedTimestamp);
    float result = rl::Lerp(previousKfVal, keyframes[kfId].value, amountOnCurve);
    return result;
}

#ifdef DEBUG_TOOLS

void CAnimation::debugGui()
{
    if(curveNeedsUpdate)
    {
        computeCurve();
        curveNeedsUpdate = false;
    }
    ImGui::PushID(DEBUG_GUI_ID(CAnimation));
    if(ImGui::CollapsingHeader("Animation"))
    {
        float kfValue = getCurrentKeyframeValue();
        ImGui::DragFloat("Time", &time, 0.1f, 0.f, 3600.f, "%.2f sec");
        ImGui::DragFloat("Keyframe value", &kfValue, 0.1f, 0.f, 1.f, "%.3f");
        if(ImGui::Button("Play"))
        {
            play();
        }
        ImGui::SameLine();
        if(ImGui::Button("Pause"))
        {
            pause();
        }

        if(ImPlot::BeginPlot("##Curve", ImVec2(-1, 100)))
        {
            ImPlot::SetupAxisLimitsConstraints(ImAxis_Y1, 0.f, 1.f);
            ImPlot::SetupAxisLimitsConstraints(ImAxis_X1, 0.f, FLT_MAX);
            ImPlot::SetupAxisZoomConstraints(ImAxis_Y1, 1.f, 1.f);
            ImPlot::SetupAxis(ImAxis_Y1, NULL, ImPlotAxisFlags_NoTickLabels);
            ImPlot::PlotLine("Animation value", curveXs, curveYs, 100, ImPlotItemFlags_NoLegend);
            ImPlot::EndPlot();
        }
    }
    ImGui::PopID();
}

void CAnimation::computeCurve()
{
    if(!isCurveInit)
    {
        float duration = getDuration();
        float pointTime = duration / 100; // for 100 points
        curveXs = new float[100];
        curveYs = new float[100];
        for(int i = 0; i <= 100; ++i)
        {
            curveXs[i] = static_cast<float>(i) * pointTime;
            curveYs[i] = getKeyframeValue(static_cast<float>(i) * pointTime);
        }
        isCurveInit = true;
    }
}

/*ImPlotPoint CAnimation::pointGetter(int idx, void* userData)
{
    keyframes
    return ImPlotPoint();
}*/

#endif


float Easing::Linear(float x)
{
    return x;
}

float Easing::SineIn(float x)
{
    return 1 - std::cos((x * PI) / 2);
}

float Easing::SineOut(float x)
{
    return std::sin((x * PI) / 2);
}

float Easing::SineInOut(float x)
{
    return -(std::cos(PI * x) - 1) / 2;
}

float Easing::CubicIn(float x)
{
    throw;
}

float Easing::CubicOut(float x)
{
    throw;
}

float Easing::CubicInOut(float x)
{
    throw;
}
