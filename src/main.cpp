#include <memory>

#define RAYMATH_IMPLEMENTATION
#include "raylib_namespace.h"
#undef RAYMATH_IMPLEMENTATION

#include "Game.h"

int main() {
    const int screenWidth = 1200;
    const int screenHeight = 750;
    std::unique_ptr<Game> pGame = std::make_unique<Game>();
    rl::InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    //rl::SetTargetFPS(60);
    pGame->exec();
    return 0;
}
