#pragma once

#include "component/CDrag.h"

#define DRAG_ONLY_ONE

class SDrag
{
public:
    void update()
    {
        auto drags = ComponentStore::getAll<CDrag>();
        // We're iterating on reverse direction so that the top z rendered entities are updated first
        for(auto entityIt = drags.rbegin(); entityIt != drags.rend(); ++entityIt)
        {
            for(const auto& comp : entityIt->second)
            {
#ifdef DRAG_ONLY_ONE
                isDraggingThisFrame = isDraggingThisFrame || static_cast<CDrag*>(comp)->update();
#else
                isDraggingThisFrame |= static_cast<CDrag*>(comp)->update();
#endif
            }
        }
    }

    void postUpdate()
    {
        isDraggingThisFrame = false;
    }

private:
    bool isDraggingThisFrame = false;
};