#pragma once

#include <vector>
#include <functional>
#include "Entity.h"

using FleetMembers = std::vector<EntityId*>;

class IFleetFormation
{
public:
    virtual void compute(const FleetMembers& fleet) = 0;
};

class IFleetTraversal
{
public:
    virtual void compute(const FleetMembers& fleet) = 0;
};

class Fleet
{
public:
    std::vector<EntityId*> FleetMembers;
    IFleetFormation* Formation;
    IFleetTraversal* Traversal;
};

class SFleet
{
    std::vector<Fleet*> _fleetList;
};
