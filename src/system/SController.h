#pragma once

#include "SDebug.h"

class IControllerState
{
public:
    IControllerState() = default;
    virtual ~IControllerState() = default;

    [[nodiscard]] virtual IControllerState* clone() const = 0;

#ifdef DEBUG_TOOLS
    virtual void debugMenu() = 0;
    virtual void debugFps() = 0;
    virtual void debugEntityList() = 0;
    virtual void debugEntityExplorer() = 0;
    virtual void debugImGuiDemo() = 0;
#endif
};

template<class Derived>
class ControllerState : public IControllerState
{
public:
    ControllerState() = default;
    ControllerState(const ControllerState& state) = default;
    ~ControllerState() override = default;

    [[nodiscard]] IControllerState* clone() const override
    {
        return new Derived(static_cast<const Derived&>(*this));
    }

#ifdef DEBUG_TOOLS
    void debugMenu() override
    {
        SDebug::switchMainMenu();
    }

    void debugFps() override
    {
        SDebug::switchFpsDisplay();
    }

    void debugEntityList() override
    {
        SDebug::switchEntityList();
    }

    void debugEntityExplorer() override
    {
        SDebug::switchEntityExplorer();
    }

    void debugImGuiDemo() override
    {
        SDebug::switchImGuiDemo();
    }
#endif
};

class BaseControllerState : public ControllerState<BaseControllerState>
{};

class SController
{
public:
    SController() : m_pState(new BaseControllerState())
    {}

    SController(const SController& controller) : m_pState(controller.m_pState->clone())
    {}

    SController& operator=(const SController& controller)
    {
        if(this == &controller)
            return *this;

        m_pState = controller.m_pState->clone();
        return *this;
    }

    ~SController()
    {
        delete m_pState;
        m_pState = nullptr;
    }

    void update()
    {
#ifdef DEBUG_TOOLS
        if((rl::IsKeyDown(rl::KEY_LEFT_CONTROL) || rl::IsKeyDown(rl::KEY_RIGHT_CONTROL)) && rl::IsKeyPressed(rl::KEY_F1))
            m_pState->debugMenu();
        else if(rl::IsKeyPressed(rl::KEY_F1))
            m_pState->debugFps();
        else if(rl::IsKeyPressed(rl::KEY_F2))
            m_pState->debugEntityList();
        else if(rl::IsKeyPressed(rl::KEY_F3))
            m_pState->debugEntityExplorer();
        else if(rl::IsKeyPressed(rl::KEY_F4))
            m_pState->debugImGuiDemo();
#endif
    }

    void switchState(IControllerState* newState)
    {
        if(newState != nullptr)
        {
            delete m_pState;
            m_pState = newState;
        }
    }

private:
    IControllerState* m_pState;
};
