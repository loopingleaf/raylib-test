#pragma once

#include <vector>
#include "ComponentStore.h"

enum class StackingDirection
{
    VERTICAL,
    HORIZONTAL,
};

struct Sides
{
    int left = 0;
    int top = 0;
    int right = 0;
    int bottom = 0;
};

class HudElement
{
public:
    /// Defines the y position of the element, relatively to the parent root position
    int* x;

    /// Defines the y position of the element, relatively to the parent root position
    int* y;

    int* width;

    int* height;

    /// Outside margins. Change position from the parent element, or from the root position if it's the root element.
    Sides margin;

    /// Inside margins. Change position of the children elements.
    Sides padding;

    /// Direction in which children needs to stack
    StackingDirection stackingDirection = StackingDirection::VERTICAL;

private:
    /// Parent element.
    HudElement* parent;

    /// Children elements.
    std::vector<HudElement*> children;

    /// Convenient offset
    int childOffset;

public:
    HudElement() = delete;
    HudElement(const HudElement& other) = delete;
    explicit HudElement(int* x, int* y, int* width, int* height, const Sides& margin, const Sides& padding,
                        const StackingDirection& direction = StackingDirection::VERTICAL);
    explicit HudElement(int* x, int* y, int* width, int* height, const Sides& margin, const Sides& padding,
                        HudElement* parent, const StackingDirection& direction = StackingDirection::VERTICAL);
    ~HudElement() = default;

    void addChild(HudElement* child)
    {
        children.push_back(child);
        child->parent = this;
    }

    /// Update the positioning of elements from the root. The tree iteration method is neither BFS nor DFS;
    void updateLayout();

private:
    /// Return the top hud element parent
    HudElement* getRoot();

    /// Update the layout, starting from fromNode
    static void updateLayout(HudElement* fromNode);
};

/// Define an easy to setup HUD needing a layout with relative positioning over parents, children and neighbours.
/*class SHudView
{
    friend struct HudElement;

    HudElement* rootElement;

public:
    explicit SHudView();
    ~SHudView();

    HudElement& addRoot(int& x, int& y, const Sides& padding);

    HudElement& addElement(int& x, int& y, const Sides& margin, const Sides& padding, HudElement& parent);

    /// Update the positioning of object. Usually called only once, after setting the HUD layout.
    /// If you want to change the positioning of the elements individually after that, mind changing the component
    /// directly instead.
    void updateLayout();
};*/
