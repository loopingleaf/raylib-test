#include "SHud.h"

HudElement::HudElement(int *x, int *y, int* width, int* height, const Sides &margin, const Sides &padding,
                       const StackingDirection& direction)
        : x(x), y(y), width(width), height(height), margin(margin), padding(padding), parent(nullptr)
{}

HudElement::HudElement(int *x, int *y, int* width, int* height, const Sides &margin, const Sides &padding,
                       HudElement *parent, const StackingDirection& direction)
    : x(x), y(y), width(width), height(height), margin(margin), padding(padding), parent(parent)
{}

HudElement* HudElement::getRoot()
{
    return parent == nullptr ? this : parent->getRoot();
}

void HudElement::updateLayout()
{
    updateLayout(getRoot());
}

void HudElement::updateLayout(HudElement* fromNode)
{
    fromNode->childOffset = 0;
    int x = 0;
    int y = 0;
    if(fromNode->parent != nullptr)
    {
        x = fromNode->parent->padding.left;
        y = fromNode->parent->padding.top;
        switch(fromNode->parent->stackingDirection)
        {
            case StackingDirection::VERTICAL:
                y += fromNode->parent->childOffset;
                y += fromNode->margin.top;
                fromNode->parent->childOffset += *fromNode->height + fromNode->margin.bottom;
                break;
            case StackingDirection::HORIZONTAL:
                x += fromNode->parent->childOffset;
                x += fromNode->margin.left;
                fromNode->parent->childOffset += *fromNode->width + fromNode->margin.right;
                break;
        }
    }

    *fromNode->x = x;
    *fromNode->y = y;

    if(fromNode->children.size() > 0)
    {
        *fromNode->width = fromNode->padding.top + fromNode->padding.bottom;
        *fromNode->height = fromNode->padding.left + fromNode->padding.right;
    }
    for(auto it : fromNode->children)
    {
        updateLayout(it);
        switch(fromNode->stackingDirection)
        {
            case StackingDirection::VERTICAL:
                *fromNode->height += *it->height + it->margin.top + it->margin.bottom;
                if(*fromNode->width < *it->width + it->margin.left + it->margin.right)
                    *fromNode->width = *it->width + it->margin.left + it->margin.right
                            + fromNode->padding.left + fromNode->padding.right;
                break;
        }
    }
}
