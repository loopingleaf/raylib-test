#pragma once

#if (DEBUG_LEVEL >= 2)

#include <climits>
#include <algorithm>
#include "raylib_namespace.h"
#include "Macros.h"
#include "Entity.h"
#include "Color.h"
#include "component/CDebugInfo.h"

#define DEBUG_GUI_ID(NAME) (#NAME + stringId()).c_str()

enum class Display : unsigned char
{
    NONE            = 0,
    MAIN_MENU       = 1 << 0,
    FPS_COUNTER     = 1 << 1,
    ENTITY_LIST     = 1 << 2,
    ENTITY_EXPLORER = 1 << 3,
    IMGUI_DEMO      = 1 << 4,

    ALL             = UCHAR_MAX
}; FLAG(Display)

class SDebug
{
public:
    static void draw()
    {
        if(Display::MAIN_MENU == (display & Display::MAIN_MENU))
            showMainMenu();

        if(Display::FPS_COUNTER == (display & Display::FPS_COUNTER))
            showFpsCounter();

        if(Display::ENTITY_LIST == (display & Display::ENTITY_LIST))
            showEntityList();

        if(Display::ENTITY_EXPLORER == (display & Display::ENTITY_EXPLORER))
            showEntityExplorer();

        if(Display::IMGUI_DEMO == (display & Display::IMGUI_DEMO))
        {
            bool open = true;
            ImGui::ShowDemoWindow(&open);
            if(!open)
                switchImGuiDemo();
        }

        if(colorPickerOpened)
            ImGui::OpenPopup("DefaultColorPicker");
        colorPickerOpened = false;

        if(ImGui::BeginPopup("DefaultColorPicker"))
        {
            drawColorPicker();
            ImGui::EndPopup();
        }

        for(auto it : drawList)
        {
            it->debugDraw();
        }
    }

    static void switchFpsDisplay()
    {
        display ^= Display::FPS_COUNTER;
    }

    static void switchMainMenu()
    {
        display ^= Display::MAIN_MENU;
    }

    static void switchEntityExplorer()
    {
        display ^= Display::ENTITY_EXPLORER;
    }

    static void switchEntityList()
    {
        display ^= Display::ENTITY_LIST;
    }

    static void switchImGuiDemo()
    {
        display ^= Display::IMGUI_DEMO;
    }

    static void selectEntity(EntityId const entity)
    {
        selectedEntity = entity;
    }

    static void displayColorPicker(rl::Color* colorToUpdate)
    {
        updatedColor = colorToUpdate;
        colorPickerOpened = true;
    }

    static void addToDrawList(Component* const component)
    {
        drawList.push_back(component);
    }

    static void removeFromDrawList(Component* const component)
    {
        auto it = std::find(drawList.begin(), drawList.end(), component);
        drawList.erase(it);
    }

private:
    inline static Display display = (Display::MAIN_MENU ^ Display::ENTITY_EXPLORER ^ Display::FPS_COUNTER);
    inline static EntityId selectedEntity = ENTITY_ID_NULL;
    inline static int selectedEntityIndex = -1;
    inline static std::vector<Component*> drawList;
    inline static bool colorPickerOpened = false;
    inline static rl::Color* updatedColor;

    static void showMainMenu()
    {
        if(ImGui::BeginMainMenuBar())
        {
            if(ImGui::BeginMenu("View"))
            {
                if(ImGui::MenuItem("FPS counter", "F1"))
                    display ^= Display::FPS_COUNTER;
                if(ImGui::MenuItem("Entity list", "F2"))
                    display ^= Display::ENTITY_LIST;
                if(ImGui::MenuItem("Entity explorer", "F3"))
                    display ^= Display::ENTITY_EXPLORER;
                if(ImGui::MenuItem("ImGui demo window", "F4"))
                    display ^= Display::IMGUI_DEMO;
                ImGui::EndMenu();
            }
            ImGui::EndMainMenuBar();
        }
    }

    static void showFpsCounter()
    {
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize |
                ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
        ImGui::SetNextWindowBgAlpha(0.35f); // Transparent background
        bool open = true;
        if (ImGui::Begin("Overlay", &open, window_flags))
        {
            ImGui::Text("FPS: %i", rl::GetFPS());
        }
        ImGui::End();
    }

    static void showEntityExplorer()
    {
        bool open = true;
        if(ImGui::Begin("Entity explorer", &open))
        {
            ImGui::PushItemWidth(80.f);
            if(selectedEntity != ENTITY_ID_NULL)
            {
                auto componentList = ComponentStore::getAllInEntity(selectedEntity);
                for(auto it : componentList)
                {
                    it->debugGui();
                }
            }
            ImGui::PopItemWidth();
        }
        ImGui::End();
        if(!open)
            switchEntityExplorer();
    }

    static void showEntityList()
    {
        bool open = true;
        if(ImGui::Begin("Entity list", &open))
        {
            auto debugInfolist = ComponentStore::getAll<CDebugInfo>();
            int i = 0;
            for(auto& it : debugInfolist)
            {
                for(auto& c : it.second)
                {
                    if (ImGui::Selectable((static_cast<CDebugInfo*>(c)->getName()).c_str(),
                                          selectedEntityIndex == i))
                    {
                        selectedEntityIndex = i;
                        selectedEntity = it.first;
                    }
                    ++i;
                }
            }
        }
        ImGui::End();
        if(!open)
            switchEntityList();
    }

    static void drawColorPicker()
    {
        ImGuiColorEditFlags colorFlags = ImGuiColorEditFlags_AlphaPreview;
        ImGui::Text("Palette");
        for(int i = 0; i < Color::PaletteSize; ++i)
        {
            if(i % 8 != 0)
                ImGui::SameLine();

            if(ImGui::ColorButton(("##ColorPickerBtn" + std::to_string(i)).c_str(), Color::RlToIm(Color::Palette[i]),
                                  colorFlags, ImVec2(20, 20)))
            {
                if(updatedColor != nullptr)
                    *updatedColor = Color::Palette[i];
            }
        }

        ImGui::Separator();
        ImGui::Text("Color Picker");
        ImVec4 imColor = Color::RlToIm(*updatedColor);
        ImGui::ColorPicker4("##ColorPicker", (float*)&imColor,
                            colorFlags | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
        *updatedColor = Color::ImToRl(imColor);
    }
};

#endif
