#pragma once

#include "ComponentStore.h"
#include "component/CShape.h"

class SShape
{
public:
    void draw()
    {
        auto entities = ComponentStore::getAll<CShape>();
        for(const auto& entityIt : entities)
        {
            for(auto component : entityIt.second)
            {
                static_cast<CShape*>(component)->draw();
            }
        }
    }
};