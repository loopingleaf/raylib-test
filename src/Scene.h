#pragma once

#include <vector>
#include "Entity.h"
#include "system/SDebug.h"
#include "system/SController.h"
#include "system/SDrag.h"
#include "system/SShape.h"
#include "entity/Player.h"
#include "entity/Window.h"
#include "system/SHud.h"

class IScene
{
public:
    virtual ~IScene() = default;

    virtual void init() = 0;
    virtual void draw() = 0;
#if (DEBUG_LEVEL >= 2)
    virtual void debugDraw() = 0;
#endif
    virtual void update(const double& dt) = 0;
    virtual IScene* switchScene() = 0;
};

class Scene : public IScene
{
public:
    ~Scene() override = default;

    void init() override
    {

    }

    void draw() override
    {

    }

#if (DEBUG_LEVEL >= 2)
    void debugDraw() override
    {
        SDebug::draw();
    }
#endif

    void update(const double& dt) override
    {

    }

    IScene* switchScene() override
    {
        return nullptr;
    }
};

class TestScene : public Scene
{
    EntityId m_player;
    EntityId m_focal;
    EntityId window;
    SDrag dragSystem;
    SShape shapeSystem;

public:
    explicit TestScene() {}
    ~TestScene() override = default;

    void init() override
    {
        m_focal = Entity::Player();
        auto t = ComponentStore::get<CTransform>(m_focal);
        t->position = rl::Vector2 { 0.f, 0.f};
        m_player = Entity::Player();

        window = Entity::Window();
        auto wt = ComponentStore::get<CTransform>(window);
        auto ft = ComponentStore::get<CFrame>(window);
        wt->position = { static_cast<float>((rl::GetScreenWidth() - ft->width) / 2),
                         static_cast<float>((rl::GetScreenHeight() - ft->height) / 2) };

        auto dialog = Entity::DialogBox();

#if (DEBUG_LEVEL >= 2)
        SDebug::selectEntity(dialog);
#endif
    }

    void draw() override
    {
        dragSystem.update();
        shapeSystem.draw();
        auto table = ComponentStore::getAll<CDrawable>();
        for(const auto& it : table)
        {
            for(const auto& c : it.second)
                static_cast<CDrawable*>(c)->draw();
        }
        auto tableA = ComponentStore::getAll<CAnimation>();
        for(const auto& it2 : tableA)
        {
            for(const auto& cA : it2.second)
                static_cast<CAnimation*>(cA)->update(rl::GetFrameTime());
        }
        dragSystem.postUpdate();
    }
};