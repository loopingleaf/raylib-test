#pragma once

#include <memory>
#include "Scene.h"
#include "entity/Player.h"
#include "system/SGravitation.h"
#include "system/SController.h"

class Game
{
public:
    Game() : m_scene(nullptr) {}

    ~Game()
    {
        delete m_scene;
        m_scene = nullptr;
        /*delete m_pPlayer;
        m_pPlayer = nullptr;*/
    }

    void exec();

private:
    /*EPlayer* m_pPlayer;
    EPlayer* m_focal;
    SGravitation m_gravSystem;*/
    SController controller;
    IScene* m_scene;

    void update(double dt);

    void draw();
};
