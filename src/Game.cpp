#include "Game.h"
#include "raylib_namespace.h"
#include "implot.h"

void Game::exec()
{
    double previousTime = 0.;
    double dt = 0.;
    double accumulator = 0.;
    double fixedDt = 0.008;
    m_scene = new TestScene();
    m_scene->init();
    controller = SController();

#if (DEBUG_LEVEL == 0)
    rl::SetExitKey(rl::KEY_NULL);
#endif
#ifdef DEBUG_TOOLS
    rl::rlImGuiSetup(true);
    ImPlot::CreateContext();
#endif

    while (!rl::WindowShouldClose())    // Detect window close button or ESC key
    {
        dt = rl::GetTime() - previousTime;
        previousTime = rl::GetTime();
        accumulator += dt;
        controller.update();
        while(accumulator >= fixedDt)
        {
            update(fixedDt);
            accumulator -= fixedDt;
        }
        rl::BeginDrawing();
        draw();
        rl::EndDrawing();
    }

#ifdef DEBUG_TOOLS
    ImPlot::DestroyContext();
    rl::rlImGuiShutdown();
#endif
    rl::CloseWindow();
}

void Game::update(double dt)
{
    m_scene->update(dt);
}

void Game::draw()
{
    rl::ClearBackground(rl::BLACK);

    if(m_scene != nullptr)
    {
        m_scene->draw();

#if (DEBUG_LEVEL >= 2)
        rl::rlImGuiBegin();
        {
            m_scene->debugDraw();
            bool open = true;
            ImPlot::ShowDemoWindow(&open);
        }
        rl::rlImGuiEnd();
#endif

    }
}
