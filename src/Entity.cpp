#include "Alias.h"
#include "Entity.h"

#if (DEBUG_LEVEL >= 2)
#include "component/CDebugInfo.h"
#endif

namespace EntityBuilder
{
    namespace
    {
        EntityId currentEntityId = ENTITY_ID_NULL;
    }

    EntityId create(const std::string& name)
    {
        EntityId result = ++currentEntityId;
#if(DEBUG_LEVEL >= 2)
        auto debugInfo = ComponentStore::build<CDebugInfo>(result);
        debugInfo->setName(name);
#endif
        return result;
    }
}
