/**
 * Inspired by http://Entity-systems.wikidot.com/test-for-parallel-processing-of-components#cpp
 * and Jeffery Myers https://github.com/JeffM2501/raylib_ecs_sample/blob/main/test/components.h
 */

#pragma once

#include <map>
#include <vector>
#include <typeindex>
#include "Alias.h"

class Component;

template<typename ComponentT>
using ComponentTable = std::map<EntityId, std::vector<ComponentT*>>;

using ComponentTableBase = ComponentTable<Component>;

namespace ComponentStore
{
    void store(const ComponentId& componentId, Component* component);

    std::map<ComponentId, ComponentTableBase>& getDatabase();

    std::vector<Component*> getAllInEntity(const EntityId& entity);

    ComponentId getNextId();

    template<typename ComponentT, typename... Args>
    inline ComponentT* build(const EntityId& entity, Args&&... args)
    {
        auto component = new ComponentT(entity, std::forward<Args>(args)...);
        component->id = getNextId();
        store(ComponentT::getBaseTypeId(), component);
        return component;
    }

    template<typename ComponentT>
    inline ComponentT* get(const EntityId& entity)
    {
        ComponentId id = ComponentT::getBaseTypeId();
        auto database = getDatabase();
        auto tableIt = database.find(id);
        if(tableIt == database.end())
            return nullptr;

        auto table = tableIt->second;
        auto componentListIt = table.find(entity);
        if(componentListIt == table.end() || componentListIt->second.empty())
            return nullptr;

        return static_cast<ComponentT*>(componentListIt->second[0]);
    }

    template<typename ComponentT>
    ComponentTableBase& getAll()
    {
        return getDatabase()[ComponentT::getBaseTypeId()];
    }

    template<typename ComponentT>
    void getAll(ComponentTable<ComponentT>& outResult)
    {
        ComponentTableBase result = getDatabase()[ComponentT::getBaseTypeId()];
        outResult = ComponentTable<ComponentT>(result.size());
        for(auto entityIt : result)
        {
            for(auto componentIt : entityIt.second)
            {
                outResult[entityIt] = static_cast<ComponentT>(componentIt);
            }
        }
    }
};

/*class ComponentStore
{
public:
    ComponentStore() = delete;
    ~ComponentStore() = delete;

    template<typename ComponentType, typename... Args>
    static ComponentType* build(const EntityId& entity, Args&&... args)
    {
        auto component = new ComponentType(entity, std::forward<Args>(args)...);
        store(entity, component);
        return component;
    }

    template<typename ComponentType>
    static void store(const EntityId& entity, ComponentType* component)
    {
        auto pair = std::pair<EntityId, ComponentType*>(entity, component);
        m_entityComponents<ComponentType>.insert(pair);
    }

    template<typename ComponentType>
    static ComponentType* getComponent(const EntityId& entity)
    {
        return m_entityComponents<ComponentType>.find(entity)->second;
    }

    template<typename ComponentType>
    static ComponentType* getComponent(const Component& component)
    {
        return m_entityComponents<ComponentType>.find(component.getEntityId())->second;
    }

    template<typename ComponentType>
    static ComponentType* getComponent(const Component* component)
    {
        return m_entityComponents<ComponentType>.find(component->getEntityId())->second;
    }

    template<typename ComponentType>
    static void get(const EntityId& entity, std::vector<ComponentType*>& components)
    {
        auto range = m_entityComponents<ComponentType>.equal_range(entity);
        for(auto it = range.first; it != range.second; ++it)
        {
            components.push_back(it->second);
        }
    }

    template<typename ComponentType>
    static const std::multimap<EntityId, ComponentType*>& getAll()
    {
        return m_entityComponents<ComponentType>;
    }

    /// Destroys all the components of the given Entity with the store's type
    /// @param entity The Entity to destroy components from
    template<typename ComponentType>
    static void destroyComponent(const EntityId& entity)
    {
        auto found = m_entityComponents<ComponentType>.equal_range(entity);
        if(found.first == found.second)
            return;

        for(auto it = found.first; it != found.second; ++it)
        {
            delete it->second;
            it->second = nullptr;
        }
        m_entityComponents<ComponentType>.erase(found.first, found.second);
    }

    template<typename ComponentType>
    static void destroyComponent(const EntityId& entity, ComponentType* component) {
        auto found = std::find_if(
                m_entityComponents<ComponentType>.begin(), m_entityComponents<ComponentType>.end(),
                    [&](std::pair<EntityId, ComponentType *> pair) {
                    return pair.first == entity && pair.second == component;
                }
        );
        if (found == m_entityComponents<ComponentType>.end())
            return;

        delete found->second;
        found->second = nullptr;
        m_entityComponents<ComponentType>.erase(found);
    }

private:
    template<typename ComponentType>
    inline static std::multimap<EntityId, ComponentType*> m_entityComponents
        = std::multimap<EntityId, ComponentType*>();

    inline static std::multimap<EntityId, Component*> m_entityComponentsAll
        = std::multimap<EntityId, Component*>();
};*/

/*template<typename ComponentType>
std::multimap<EntityId, ComponentType*> ComponentStore<ComponentType>::m_entityComponents
    = std::multimap<EntityId, ComponentType*>();*/
