#pragma once

#include "raylib_namespace.h"
#include "Entity.h"
#include "Color.h"
#include "Maths.h"
#include "component/CShape.h"
#include "component/CFrame.h"
#include "component/CDrag.h"
#include "component/CDragBack.h"
#include "component/CLoading.h"
#include "component/CText.h"
#include "component/CAnimation.h"
#include "system/SHud.h"

namespace Entity
{
    inline EntityId Window()
    {
        EntityId id = EntityBuilder::create("Window");
        auto transform = ComponentStore::build<CTransform>(id);
        transform->position = { 0.f, 0.f };

        auto frame = ComponentStore::build<CFrame>(id);
        frame->borderColor = Color::White;
        frame->color = Color::Background;
        frame->outlineBorderColor = Color::DarkRed;
        HudElement frameLayout = HudElement(&frame->offsetX, &frame->offsetY, &frame->width, &frame->height, Sides(),
                                            { 10, 10, 10, 10 });

        auto text = ComponentStore::build<CText>(id);
        text->text = "Loading...";
        rl::Vector2 textSize = text->getSize();
        int textWidth = static_cast<int>(textSize.x);
        int textHeight = static_cast<int>(textSize.y);
        HudElement textLayout = HudElement(&text->offsetX, &text->offsetY, &textWidth, &textHeight, Sides(), Sides());
        frameLayout.addChild(&textLayout);

        auto l = ComponentStore::build<CLoading>(id);
        //l->margin = { 10, 10, 10, 10 };
        l->size = 80;
        rl::Vector2 lSize = l->getSize();
        int lw = static_cast<int>(lSize.x);
        int lh = static_cast<int>(lSize.y);
        HudElement lLayout = HudElement(&l->offsetX, &l->offsetY, &lw, &lh, { 0, 4, 0, 0 }, Sides());
        frameLayout.addChild(&lLayout);

        frameLayout.updateLayout();

        return id;
    }

    inline EntityId DialogBox()
    {
        EntityId id = EntityBuilder::create("DialogBox");
        auto transform = ComponentStore::build<CTransform>(id);
        int height = 140;
        int width = 750;
        transform->position = rl::Vector2 { static_cast<float>(rl::GetScreenWidth() - width) / 2.f,
                                            static_cast<float>(rl::GetScreenHeight() - height) - 20.f };

        auto frame = ComponentStore::build<CFrame>(id);
        frame->height = height;
        frame->width = width;
        frame->color = Color::Background;
        frame->borderColor = Color::White;
        frame->outlineBorderColor = Color::DarkRed;

        auto frame2 = ComponentStore::build<CFrame>(id);
        frame2->width = frame2->height = 120;
        frame2->offsetX = frame2->offsetY = 10;
        frame2->color = Color::Transparent;
        frame2->borderColor = Color::White;
        frame2->outlineBorderColor = Color::DarkRed;

        ComponentStore::build<CDragBack>(id);

        auto drag = ComponentStore::build<CDrag>(id);
        drag->dragZone = RectangleI { 0, 0, width, height };
        drag->deadZone = 25.f;
        drag->onDrag([](EntityId id, int x, int y) {
            auto t = ComponentStore::get<CTransform>(id);
            auto f = ComponentStore::get<CFrame>(id);
            auto dragBack = ComponentStore::get<CDragBack>(id);
            dragBack->computeXFrame(x);
            dragBack->computeYFrame(y);
            t->position.x += x;
            t->position.y += y;
            rl::Vector2 dragBackFrame = Maths::clamp(t->position, { 0.f, 0.f },
                                                     { static_cast<float>(rl::GetScreenWidth() - f->width),
                                                       static_cast<float>(rl::GetScreenHeight() - f->height) });
            if(dragBack->x == 0 && dragBackFrame.x != 0)
                dragBack->x = dragBackFrame.x;
            if(dragBack->y == 0 && dragBackFrame.y != 0)
                dragBack->y = dragBackFrame.y;
        });
        drag->onDrop([](EntityId id, int dropPositionX, int dropPositionY) {
            ComponentStore::get<CDragBack>(id)->reset();
        });

        auto animation = ComponentStore::build<CAnimation>(id);
        Keyframe kf = { .2f, 1.f, &Easing::SineIn };
        animation->addKeyframe(kf);
        animation->onPlay([](EntityId en, float v) {
            auto f = ComponentStore::get<CFrame>(en);
            f->height = rl::Lerp(0.f, 140.f, v);
        });
        animation->play();

        return id;
    }
};