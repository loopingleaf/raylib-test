#pragma once

#include <memory>
#include "raylib_namespace.h"
#include "Entity.h"
#include "component/CTransform.h"
#include "component/CCelestial.h"
#include "component/CEllipse.h"

namespace Entity
{
    inline EntityId Player(const float& x = 0.f, const float& y = 0.f)
    {
        auto id = EntityBuilder::create("Player");
        auto t = ComponentStore::build<CTransform>(id);
        t->position = rl::Vector2 { x, y };
        auto t2 = ComponentStore::build<CTransform>(id);
        t2->position = rl::Vector2 { 20.f, 18.f };
        return id;
    }
}
