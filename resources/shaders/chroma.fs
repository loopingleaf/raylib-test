#version 330

in vec2 fragTexCoord;
in vec4 fragColor;

uniform sampler2D texture0;
uniform vec4 colDiffuse;

out vec4 finalColor;

uniform float time;

void main()
{
    // distance from center of image, used to adjust blur
    //float d = length(fragTexCoord - vec2(0.5,0.5));

    // blur amount
    float blur = 0.0f;
    blur = (1.0f + sin(time * 6.0f)) * 0.5f;
    blur *= 1.0f + sin(time * 16.0f) * 0.5f;
    blur = pow(blur, 3.0f);
    blur *= 0.002f;
    // reduce blur towards center
    //blur *= d;

    // final color
    vec4 col;
    col = texture( texture0, fragTexCoord );
    if(texture( texture0, vec2(fragTexCoord.x + blur, fragTexCoord.y) ).a != 0.0f && blur >= 0.0001f)
    {
        col.a = 1.f;
        col.r = 1.f;
        col.g = 0.f;
        col.b = 0.f;
    }
    col.b = texture( texture0, vec2(fragTexCoord.x - blur, fragTexCoord.y) ).b;
    col.a += texture( texture0, vec2(fragTexCoord.x + blur, fragTexCoord.y) ).a;
    col.a += texture( texture0, vec2(fragTexCoord.x - blur, fragTexCoord.y) ).a;

    // scanline
    /*float scanline = sin(uv.y*800.0)*0.04;
    col -= scanline;*/

    // vignette
    //col *= 1.0 - d * 0.5;

    finalColor = col * colDiffuse * fragColor;
}
