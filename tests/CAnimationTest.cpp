#define RAYMATH_IMPLEMENTATION

#include "raylib_namespace.h"

#undef RAYMATH_IMPLEMENTATION

#include "gtest/gtest.h"
#include "component/CCelestial.h"
#include "component/CTransform.h"
#include "component/CAnimation.h"

class CAnimationTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        auto e = EntityBuilder::create("TestEntity");
        animationComponent = ComponentStore::build<CAnimation>(e);
        animationComponent->play();
    }

    CAnimation* animationComponent;
};

TEST_F(CAnimationTest, update_NoFirstKeyframe_ShouldStartAtZero)
{
    // Arrange
    Keyframe kfWithNonZeroDuration = { 1.f, 1.f, &Easing::Linear };
    animationComponent->addKeyframe(kfWithNonZeroDuration);
    float result = 14.f; // We initialize it at a non 0 value to avoid false positive
    animationComponent->onPlay([&result](EntityId en, float v) {
        result = v;
    });

    // Act
    animationComponent->update(0.f);

    // Assert
    ASSERT_EQ(0.f, result);
}

TEST_F(CAnimationTest, update_FirstKeyframeTimestampBeingZero_ShouldStartAtKeyframeValue)
{
    // Arrange
    Keyframe kfWithDurationEqualsZero = { 0.f, 0.8f, &Easing::Linear };
    animationComponent->addKeyframe(kfWithDurationEqualsZero);
    float result = 14.f;
    animationComponent->onPlay([&result](EntityId en, float v) {
        result = v;
    });

    // Act
    animationComponent->update(0.f);

    // Assert
    ASSERT_EQ(0.8f, result);
}

TEST_F(CAnimationTest, update_FirstKfTimestampBeingZeroAndOneKf_ShouldEndWithLastKfValue)
{
    // Arrange
    Keyframe kfWithDurationEqualsZero = { 0.0f, 0.8f, &Easing::Linear };
    animationComponent->addKeyframe(kfWithDurationEqualsZero);
    float result = 14.f;
    animationComponent->onPlay([&result](EntityId en, float v) {
        result = v;
    });

    // Act
    animationComponent->update(0.2f);

    // Assert
    ASSERT_EQ(0.8f, result);
}

TEST_F(CAnimationTest, update_DurationDifferentFromZero_ShouldEndWithLastKfValue)
{
    // Arrange
    Keyframe kfWithNonZeroDuration = { 0.2f, 0.8f, &Easing::Linear };
    animationComponent->addKeyframe(kfWithNonZeroDuration);
    float result = 14.f;
    animationComponent->onPlay([&result](EntityId en, float v) {
        result = v;
    });

    // Act
    animationComponent->update(0.2f);

    // Assert
    ASSERT_EQ(0.8f, result);
}

TEST_F(CAnimationTest, update_ZeroToOneKfValue_ShouldGoFromZeroToOne)
{
    // Arrange
    const float EXPECTED_RESULT_1 = 0.f, EXPECTED_RESULT_2 = .4f, EXPECTED_RESULT_3 = .8f, EXPECTED_RESULT_4 = 1.f;
    const float TIME_1 = 0.f, TIME_2 = .4f, TIME_3 = .4f, TIME_4 = .2f;
    const float DURATION = 1.f;
    float result1, result2, result3, result4;
    float currentResult;
    Keyframe kfWithValueOne = { DURATION, 1.f, &Easing::Linear };
    animationComponent->addKeyframe(kfWithValueOne);
    animationComponent->onPlay([&currentResult](EntityId en, float v) {
        currentResult = v;
    });

    // Act
    animationComponent->update(TIME_1);
    result1 = currentResult;
    animationComponent->update(TIME_2);
    result2 = currentResult;
    animationComponent->update(TIME_3);
    result3 = currentResult;
    animationComponent->update(TIME_4);
    result4 = currentResult;

    // Assert
    ASSERT_NEAR(EXPECTED_RESULT_1, result1, 0.001f);
    ASSERT_NEAR(EXPECTED_RESULT_2, result2, 0.001f);
    ASSERT_NEAR(EXPECTED_RESULT_3, result3, 0.001f);
    ASSERT_NEAR(EXPECTED_RESULT_4, result4, 0.001f);
}

TEST_F(CAnimationTest, update_OneToZeroKfValue_ShouldGoFromOneToZero)
{
    // Arrange
    const float EXPECTED_RESULT_1 = 1.f, EXPECTED_RESULT_2 = .8f, EXPECTED_RESULT_3 = .4f, EXPECTED_RESULT_4 = 0.f;
    const float TIME_1 = 0.f, TIME_2 = .2f, TIME_3 = .4f, TIME_4 = .4f;
    const float DURATION = 1.f;
    float result1, result2, result3, result4;
    float currentResult;
    Keyframe firstKfWithValueOne = { 0.f, 1.f, &Easing::Linear };
    Keyframe kfWithValueZero = { DURATION, 0.f, &Easing::Linear };
    animationComponent->addKeyframe(firstKfWithValueOne);
    animationComponent->addKeyframe(kfWithValueZero);
    animationComponent->onPlay([&currentResult](EntityId en, float v) {
        currentResult = v;
    });

    // Act
    animationComponent->update(TIME_1);
    result1 = currentResult;
    animationComponent->update(TIME_2);
    result2 = currentResult;
    animationComponent->update(TIME_3);
    result3 = currentResult;
    animationComponent->update(TIME_4);
    result4 = currentResult;

    // Assert
    ASSERT_NEAR(EXPECTED_RESULT_1, result1, 0.001f);
    ASSERT_NEAR(EXPECTED_RESULT_2, result2, 0.001f);
    ASSERT_NEAR(EXPECTED_RESULT_3, result3, 0.001f);
    ASSERT_NEAR(EXPECTED_RESULT_4, result4, 0.001f);
}

TEST_F(CAnimationTest, update_NonFirstKf_ShouldStartAtPreviousKfValue)
{
    // Arrange
    const float EXPECTED_RESULT = .5f;
    const float KF_TIME = .2f;
    const Keyframe FIRST_KEYFRAME = { KF_TIME, EXPECTED_RESULT, &Easing::Linear };
    const Keyframe SECOND_KEYFRAME = { KF_TIME, 1.f, &Easing::Linear };
    float result;
    animationComponent->addKeyframe(FIRST_KEYFRAME);
    animationComponent->addKeyframe(SECOND_KEYFRAME);
    animationComponent->onPlay([&result](EntityId en, float v) {
        result = v;
    });

    // Act
    animationComponent->update(KF_TIME);

    // Assert
    ASSERT_NEAR(EXPECTED_RESULT, result, 0.001f);
}
