project(Tests)

add_subdirectory(lib/googletest)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
include_directories(${gmock_SOURCE_DIR}/include ${gmock_SOURCE_DIR})

add_executable(tests CAnimationTest.cpp ../src/ComponentStore.cpp ../src/Entity.cpp ../src/component/CAnimation.cpp)
target_link_libraries(tests gtest gtest_main ${RLIMGUI} ${RAYLIB} opengl32 gdi32 winmm)
